package com.zoo.ihm;

import java.util.Collection;
import java.util.List;
import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.zoo.dto.AnimalDto;
import com.zoo.dto.EnclosDto;
import com.zoo.error.Erreur;
import com.zoo.service.IEnclosService;


public class MenuEnclos {


	public static void menu() {
		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
		ctx.register(SpringConfiguration.class);
		ctx.refresh();

		IEnclosService enclosService = ctx.getBean(IEnclosService.class);


		boolean vrai = true;

		while(vrai) {
			System.out.println("");
			System.out.println("------------ Menu Enclos------------");
			System.out.println("");
			System.out.println("1: ajouter un Enclos");
			System.out.println("2: afficher tous les Enclos");
			System.out.println("3: afficher un enclos par son nom");
			System.out.println("4: mise a jour enclos");
			System.out.println("5: supprimer un enclos");
			System.out.println("6: ajout responsable a enclos");
			System.out.println("7: ajout animal a enclos");
			System.out.println("8: lister animaux de l'enclos");
			System.out.println("9: fermer menu des enclos");

			System.out.println("entrer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();

			switch (choix) {
			case "1":
				System.out.println("ajouter un enclos");
				System.out.println("entrer un nom pour l'enclos");
				String label = sc.next();
				enclosService.creer(label);

				break;

			case "2":
				System.out.println("afficher tous les enclos");

				Collection<EnclosDto> listenclosdto =enclosService.lister();
				for (EnclosDto enclosDto : listenclosdto) {
					System.err.println(enclosDto);
				}
				break;

			case "3":
				System.out.println("afficher un enclos par son nom");
				System.out.println("entrer le nom de l'enclos a afficher");
				String nom = sc.next();
				System.out.println(enclosService.enclosParNom(nom));
				break;

			case "4":
				System.out.println("mettre a jour une enclos");
				System.out.println("entrer le nom de l'enclos a mettre a jour");
				String oldName = sc.next();
				System.out.println("entrer le nouveau nom pour l'enclos");
				String newName = sc.next();
				enclosService.miseAjourNom(oldName, newName);
				break;

			case "5":
				System.out.println("Supprimer un enclos");
				System.out.println("entrer le nom de l' enclos a supprimer");
				nom = sc.next();
				System.out.println("entrer l'ID de l' enclos a supprimer");
				int id2 = sc.nextInt();
				Erreur supprime=enclosService.supprimer(nom,id2);
				System.out.println(supprime.getNom());
				break;

			case "6":
				System.out.println("ajouter un responsable a un enclos");
				System.out.println("Veuillez saisir l'id du responsable");
				int idResp = sc.nextInt();
				System.out.println("Veuillez saisir l'id de l'enclos");
				int idEnclos = sc.nextInt();
				Erreur e=enclosService.ajoutResponsableEnclos(idEnclos, idResp);
				if(e!=null) {
					System.err.println(e.getNom());
				}

				break;

			case "7":
				System.out.println("ajouter un animal a un enclos");
				System.out.println("Veuillez saisir l'id de l'animal");
				int idAnimal = sc.nextInt();
				System.out.println("Veuillez saisir l'id de l'enclos");
				idEnclos = sc.nextInt();
				 e=enclosService.ajoutAnimalEnclos(idEnclos, idAnimal);
				 if(e!=null) {
					 System.err.println(e.getNom());
				 }
				 else {
					 System.err.println("animal a bien ete ajoute a l'enclos");
				 }
				 
				break;

			case "8":
				System.out.println("ajouter un animal a un enclos");
				System.out.println("Veuillez saisir l'id de l'enclos");
				idEnclos = sc.nextInt();
				List<AnimalDto> list=enclosService.listerAnimalEnclos(idEnclos);
				for (AnimalDto animalDto : list) {
					System.out.println(animalDto);

				}
				break;

			case "9":
				System.out.println("fin du programme");
				vrai = false;
				break;

			default:
				System.out.println("le choix entrer n'existe pas");
				break;
			}
		}
	}

}
