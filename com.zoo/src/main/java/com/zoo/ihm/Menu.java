package com.zoo.ihm;

import java.util.Scanner;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.afpa.conf.SpringConfiguration;
import com.zoo.service.IResponsableService;




public class Menu {

	public static void main(String[] args) {
		

		boolean vrai = true;
		
		while(vrai) {
			System.out.println("");
			System.out.println("---------- menu principal----------");
			System.out.println("");
			System.out.println("1: entrer dans le menu Animal");
			System.out.println("2: entrer dans le menu Aliment");
			System.out.println("3: entrer dans le menu Enclos");
			System.out.println("4: entrer dans le menu Responsable");
			System.out.println("5: fin");
			System.out.println("entrer votre choix");
			Scanner sc = new Scanner(System.in);
			String choix = sc.next();
			
			switch (choix) {
			case "1":
				MenuAnimal.menu();
				break;
				
			case "2":
				MenuAliment.menu();
				break;
				
			case "3":
				MenuEnclos.menu();
				break;
				
			case "4":
				MenuResponsable.menu();
				break;
				
			case "5":
				System.out.println("fin");
				vrai = false;
				break;

			default:
				break;
			}
			
		}

	}



}
