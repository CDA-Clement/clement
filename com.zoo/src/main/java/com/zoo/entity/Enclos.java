package com.zoo.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString(of= {"id","nom","animals","responsable"})
@Table(name = "t_enclos")
@NamedQueries({
	@NamedQuery(name = "listeEnclos", query = "SELECT v FROM Enclos v")
	//@NamedQuery(name = "listVoitureByCouleurJpql", query = "select v from Enclos v where v.couleur.code = :codeCouleur"),
	//@NamedQuery(name = "listeVoitureBestPuissanceByMarque", query = "select v from Enclos v inner join Modele mo ON v.modele.code = mo.code WHERE puissance = (SELECT MAX(puissance) FROM Enclos v INNER JOIN Modele mo ON v.modele.code = mo.code)")
})
	//  @NamedNativeQuery(name = "listVoitureByCouleur", query = "select * from t_enclos where couleur = 116")
	//	@NamedQuery(name = "listeVoitureBestPuissanceByMarque", query = "SELECT v.* FROM ((t_enclos as v LEFT JOIN t_marque ma ON v.modele = ma.modeles) LEFT JOIN t_energie as e ON ") })


public class Enclos {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	private String nom;
	
	
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST },fetch = FetchType.EAGER, mappedBy = "enclos")
	private Set<Animal> animals;


	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="responsable")
	private Responsable responsable;
//	
//
//
//	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER)
//    @JoinTable(name="aliment", joinColumns={@JoinColumn(referencedColumnName="id")}
//                                        , inverseJoinColumns={@JoinColumn(referencedColumnName="id")})  
//    private Set<Aliment> aliments;
//	


}