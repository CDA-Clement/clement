package com.zoo.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.zoo.entity.Aliment;
import com.zoo.entity.Enclos;


@Repository
public interface IEnclosDao extends CrudRepository<Enclos,Integer> {
	List<Enclos> findByNomOrderByNom(String nom);
	List<Enclos> findAllByOrderByNomDesc();

}
