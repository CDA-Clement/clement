package com.zoo.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class AlimentDto {

	private Integer id;
	private String appelation;
}
