package com.zoo.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zoo.dao.IAlimentDao;
import com.zoo.dto.AlimentDto;
import com.zoo.entity.Aliment;
import com.zoo.service.IAlimentService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AlimentServiceImpl implements IAlimentService {
	
	@Autowired
	IAlimentDao alimentDao;
	
	@Override
	public AlimentDto creer(String appelation) {
		List<Aliment> aliments = this.alimentDao.findByNomOrderByNom(appelation);
		if(aliments != null && aliments.size() != 0) {
			log.error("des aliments ont deja ce appelation : " + aliments);
			return null;
		}
		Aliment aliment = Aliment.builder().nom(appelation).build();
		
		aliment = this.alimentDao.save(aliment);
		
		return AlimentDto.builder().id(aliment.getId()).appelation(aliment.getNom()).build();
	}

	@Override
	public List<AlimentDto> lister() {
		Iterator<Aliment> iterator = this.alimentDao.findAllByOrderByNomDesc().iterator();
		List<AlimentDto> lst = new ArrayList<>();
		while(iterator.hasNext()) {
			Aliment x = iterator.next();
			lst.add(AlimentDto.builder()
					.id(x.getId())
					.appelation(x.getNom())
					.build());
		}
		return lst;
	}

	@Override
	public AlimentDto alimentParNom(String n) {
		Iterator<Aliment> iterator = this.alimentDao.findByNomOrderByNom(n).iterator();
		AlimentDto res = null;
		if(iterator.hasNext()) {
			Aliment x = iterator.next();
			res = AlimentDto.builder()
					.id(x.getId())
					.appelation(x.getNom())
					.build();
		}
		return res;
	}

	@Override
	public void miseAjourNom(String oldName, String newName) {
		Iterator<Aliment> iterator = this.alimentDao.findByNomOrderByNom(oldName).iterator();
		AlimentDto res = null;
		if(iterator.hasNext()) {
			Aliment x = iterator.next();
			x.setNom(newName);
			this.alimentDao.save(x);
		}
	}

	@Override
	public void supprimer(String nom) {
		List<Aliment> aliments = this.alimentDao.findByNomOrderByNom(nom);
		if(aliments == null) {
			log.error("aucun aliment existe avec ce nom: " + nom);
			
		}
		for (Aliment aliment : aliments) {
			if(aliment.getNom().equalsIgnoreCase(nom)) {
				this.alimentDao.delete(aliment);
				System.out.println("le aliment : "+nom+" a bien ete supprim�");
			}
			
		}
		
	}


}
