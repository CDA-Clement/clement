package com.zoo.service;


import java.util.List;


import com.zoo.dto.AnimalDto;



public interface IAnimalService {
	
	AnimalDto creer(String nom);

	List<AnimalDto> lister();

	AnimalDto animalParNom(String n);

	void miseAjourNom(String oldName, String newName);

	void supprimer(String nom, Integer id);


}
