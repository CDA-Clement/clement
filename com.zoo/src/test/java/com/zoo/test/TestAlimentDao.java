package com.zoo.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import com.zoo.dao.AlimentDao;
import com.zoo.dao.EnclosDao;
import com.zoo.entity.Aliment;
import com.zoo.entity.Enclos;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;



@TestMethodOrder(value = OrderAnnotation.class)
public class TestAlimentDao {
	static AlimentDao dao;
	static EnclosDao daoE;

	@BeforeAll
	public static void beforeAll() {
		dao = new AlimentDao();

	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Aliment aliment;

	@Test
	@Order(1)
	public void ajoutAliment() {
		Aliment m = Aliment.builder().nom("Manioc").build();
		m = dao.add(m);
		assertNotNull(m);
		assertNotEquals(0, m.getId());
		aliment = m;
	}

	@Test
	@Order(2)
	public void miseAjourAliment() {
		aliment.setNom("Banane");
		dao.update(aliment);
		Aliment m = dao.find(aliment.getId());
		assertNotNull(m);
		assertEquals("Banane", aliment.getNom());
	}

	@Test
	@Order(3)
	public void trouverAliment() {
		Aliment m = dao.find(aliment.getId());
		assertNotNull(m);
		assertEquals(aliment.getId(), m.getId());
	}
	
	@Test
	@Order(4)
	public void ajoutAlimentAEnclos() {
		daoE=new EnclosDao();
		Enclos e = daoE.find(2);
		Aliment m = Aliment.builder().nom("Viande").build();
		Aliment m1 = Aliment.builder().nom("Fruit").build();


		List<Aliment> aliments = Arrays.asList(m, m1);
		e.setAliments(new HashSet<Aliment>(aliments));

		aliment = dao.add(m);
		daoE.update(e);
		assertNotNull(aliment);
		assertNotEquals(0, aliment.getId());
		daoE.close();
	}

	@Test
	@Order(5)
	public void listerAliment() {
		Collection<Aliment> aliment = dao.findAllNamedQuery("listeAliment");
		assertNotEquals(0, aliment.size());
		aliment.stream().forEach(System.err::println);
	}
	@Disabled
	@Test
	@Order(6)
	public void supprimerAliment() {
		dao.remove(aliment.getId());
		Aliment m = dao.find(aliment.getId());
		assertNull(m);
	}
}
