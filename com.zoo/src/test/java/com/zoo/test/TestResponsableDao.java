package com.zoo.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import com.zoo.dao.EnclosDao;
import com.zoo.dao.ResponsableDao;
import com.zoo.entity.Enclos;
import com.zoo.entity.Responsable;

@TestMethodOrder(value = OrderAnnotation.class)
class TestResponsableDao {

	static ResponsableDao dao;
	static EnclosDao daoE;

	@BeforeAll
	public static void beforeAll() {
		dao = new ResponsableDao();

	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Responsable responsable;

	@Test
	@Order(1)
	public void ajoutResponsable() {
		Responsable a = Responsable.builder().nom("Jean").build();
		a = dao.add(a);
		assertNotNull(a);
		assertNotEquals(0, a.getMatricule());
		responsable = a;
	}

	@Test
	@Order(2)
	public void miseAjourResponsable() {
		responsable.setNom("Marin");
		dao.update(responsable);
		Responsable a = dao.find(responsable.getMatricule());
		assertNotNull(a);
		assertEquals("Marin", responsable.getNom());
	}
	@Disabled
	@Test
	@Order(7)
	public void supprimerResponsable() {
		dao.remove(responsable.getMatricule());
		Responsable a = dao.find(responsable.getMatricule());
		assertNull(a);
	}

	@Test
	@Order(4)
	public void ajoutEnclosResponsable() {
	
		Responsable a = Responsable.builder().nom("PierrDe").build();
		Enclos enclos = Enclos.builder().appelation("insecte").build();
		a.setEnclos(enclos);
		responsable = dao.add(a);

		
		
		

		assertNotNull(responsable);
		assertNotEquals(0, responsable.getMatricule());
	}


	@Test
	@Order(6)
	public void listerResponsable() {
		Collection<Responsable> responsable = dao.findAllNamedQuery("listeResponsable");
		assertNotEquals(0, responsable.size());
		responsable.stream().forEach(System.err::println);

	}

	@Test
	@Order(3)
	public void trouverResponsable() {
		Responsable a = dao.find(responsable.getMatricule());
		assertNotNull(a);
		assertEquals(responsable.getMatricule(), a.getMatricule());


	}
}
